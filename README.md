# deep-video

A [fast.ai](https://www.fast.ai/) based deep learning project for video classification.

Authors: Pedro Caiafa and Gustavo Carvalho

## References

Workflow and good practices: [fast.ai:github](https://github.com/fastai)

LR Finder: [surmenok:github](https://github.com/surmenok/keras_lr_finder)

OneCycleLearn: [bckenstler:github](https://github.com/bckenstler/CLR)

I3D: [dlpbc:github](github.com/dlpbc/keras-kinetics-i3d), [deepmind:github](https://github.com/deepmind/kinetics-i3d), [googlecolab:github](https://github.com/tensorflow/hub/blob/master/examples/colab/action_recognition_with_tf_hub.ipynb)

Keras DataGenerator: [stanford:tutorial](https://stanford.edu/~shervine/blog/keras-how-to-generate-data-on-the-fly)

TfRecords: [ferreirafabio:github](https://github.com/ferreirafabio/video2tfrecord), [tomrunia:github](https://gist.github.com/tomrunia/7ef5d40639f2ae41fb71d3352a701e4a), [datlife:github](https://gist.github.com/datlife/abfe263803691a8864b7a2d4f87c4ab8), [ymedialabs-innovation:tutorial](https://medium.com/ymedialabs-innovation/how-to-use-tfrecord-with-datasets-and-iterators-in-tensorflow-with-code-samples-ffee57d298af)

Data Augmentation: [aleju:github](https://github.com/aleju/imgaug)

Video Conversion: [kenshohara:github](https://github.com/kenshohara/video-classification-3d-cnn-pytorch)

Dataset: [ucf101:dataset](http://crcv.ucf.edu/data/UCF101.php), [hmdb51:dataset](http://serre-lab.clps.brown.edu/resource/hmdb-a-large-human-motion-database/)