import numpy as np
import os
from tqdm import tqdm

import tensorflow as tf
from tensorflow.python.keras.utils import to_categorical


class TFRecordDataset():
    '''
        Converts videos to tfrecord dataset
        adapted from: 
        https://github.com/ferreirafabio/video2tfrecord
        https://gist.github.com/tomrunia/7ef5d40639f2ae41fb71d3352a701e4a
        https://gist.github.com/datlife/abfe263803691a8864b7a2d4f87c4ab8
        https://medium.com/ymedialabs-innovation/how-to-use-tfrecord-with-datasets-and-iterators-in-tensorflow-with-code-samples-ffee57d298af
    '''


    def __init__(self, data, tfrecords_path):
        self.tfrecords_path = tfrecords_path
        self.n_frames = data.input_shape[0]
        self.batch_size = data.batch_size
        self.data = data

    def _int64_feature(self, value):
        return tf.train.Feature(int64_list=tf.train.Int64List(value=[value]))

    def _bytes_feature(self, value):
        return tf.train.Feature(bytes_list=tf.train.BytesList(value=[value]))

    def _np2tfrecord(self, data, label, filename):
        """Convert video numpy array to tfrecord file"""

        # Get shapes
        num_videos, num_images, height, width, num_channels = data.shape
        
        # Setup writer
        filename = os.path.join(self.tfrecords_path, filename+'.tfrecords')
        writer = tf.python_io.TFRecordWriter(filename)

        # Loop on videos and save features
        feature = {}

        for video_count in range(num_videos):

            # Set video features
            for image_count in range(num_images):
                path = "frames/{:04d}".format(image_count)
                image = data[video_count, image_count, :, :, :]
                image = image.astype('uint8')
                image = image.tostring()
                feature[path] = self._bytes_feature(image)

            feature['height'] = self._int64_feature(height)
            feature['width'] = self._int64_feature(width)
            feature['num_images'] = self._int64_feature(num_images)
            feature['num_channels'] = self._int64_feature(num_channels)
            feature['label'] = self._int64_feature(label[video_count])

            # Save video features
            example = tf.train.Example(features=tf.train.Features(feature=feature))
            writer.write(example.SerializeToString())
            
        writer.close()

    def _unnormalize(self, x):
        x += 1.
        x *= 127.5
        x = x.astype('uint64')
        return x

    def build_tfrecords_dataset(self, generator, num_epochs=3):
        """Builds tfrecords dataset from basic dataloader outputs"""

        n_batches_per_epoch = len(generator)
        epochs_count = 1
        total_batches = num_epochs*n_batches_per_epoch-1
        pbar = tqdm(total=total_batches)
        for batch_sample, (feature, label) in enumerate(generator):
            
            # Prepare feature and label
            feature = self._unnormalize(feature)
            label = np.argmax(label, axis=1)
            
            # Save to tfrecord from basic dataloader output
            self._np2tfrecord(feature, label, 
                              'batch_{}_out_of_{}'
                              .format(batch_sample, total_batches))
            
            # Stop in the final epoch
            if (batch_sample+1) % n_batches_per_epoch == 0:
                if epochs_count >= num_epochs:
                    break
                epochs_count += 1

            # Update progress status
            pbar.update(1)

        pbar.close()

    def _parse_function(self, example_proto):
        """Creates video from images"""
        
        # Setup features dict
        feature_dict = {}
        for i in range(self.n_frames):
            feature_dict['frames/{:04d}'.format(i)] = tf.FixedLenFeature((), tf.string)
        feature_dict['height'] = tf.FixedLenFeature([], tf.int64)
        feature_dict['width'] = tf.FixedLenFeature([], tf.int64)
        feature_dict['num_images'] = tf.FixedLenFeature([], tf.int64)
        feature_dict['num_channels'] = tf.FixedLenFeature([], tf.int64)
        feature_dict['label'] = tf.FixedLenFeature([], tf.int64)

        # Parse defined features
        parsed_features = tf.parse_single_example(example_proto,
                                           features=feature_dict)
        # Unpack features
        height = parsed_features['height']
        width = parsed_features['width']
        num_images = parsed_features['num_images']
        num_channels = parsed_features['num_channels']
        label = parsed_features['label']

        # Stack frames into video
        video = []
        for i in range(self.n_frames):
            image_buffer = tf.reshape(parsed_features['frames/{:04d}'.format(i)], shape=[])
            image = tf.decode_raw(image_buffer, tf.uint8)
            image = tf.reshape(image, tf.stack([height, width, num_channels]))
            video.append(image)
        video = tf.stack(video)

        return video, label

    def _preprocess_video(self, image, label):
        '''A transformation function to preprocess raw data
        into trainable input. '''
        x = tf.cast(image, tf.float32) * (1./127.5) - 1.0
        y = tf.one_hot(tf.cast(label, tf.uint8), self.data.n_classes)
        return x, y

    def get_tfdata_generator(self, batch_size=None):
        '''Construct a data generator using tf.Dataset'''   

        # Set default batch size as set on dataloader
        if not batch_size:
            batch_size = self.batch_size

        # Get all tfrecord files
        files = os.listdir(self.tfrecords_path)
        tfrecod_files = [os.path.join(self.tfrecords_path, file) 
                         for file in files 
                         if '.tfrecords' in file]    
        # Setup dataset
        dataset = tf.data.TFRecordDataset(tfrecod_files)
        # Shuffle data
        dataset = dataset.shuffle(1000)
        # Transform and batch data at the same time
        dataset = dataset.map(self._parse_function)
        dataset = dataset.map(self._preprocess_video)
        dataset = dataset.repeat().batch(batch_size)
        dataset = dataset.prefetch(tf.contrib.data.AUTOTUNE)

        return dataset