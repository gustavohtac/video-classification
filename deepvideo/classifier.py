from tensorflow.python.keras.models import load_model
from tensorflow.python.keras.models import Model

import numpy as np
from tqdm import tqdm

from sklearn.metrics import confusion_matrix
from sklearn.metrics import classification_report

import matplotlib.pyplot as plt
import seaborn as sns


class VideoClassifier():
    '''
        fast.ai based intepreter
        workflow and good practices learned from fast.ai course
        page: https://www.fast.ai/
    '''

    def __init__(self, learner, max_frames=512):

        self.learner = learner
        self.data = learner.data
        self.model = learner.model
        self.max_frames = max_frames
        self._predict_on_test()
    
    def _split_video_in_batches(self, video, max_frames, nf=16):
        # Load video from generator
        video = self.data.test_gen.video_reader(video, 
                                    all_frames=True,
                                    max_frames=max_frames)
        # Get dimensions
        totalf, h, w, c = video.shape
        # Calculate number of inner batches
        nb = (np.ceil(totalf/nf)*nf).astype('uint64')
        # Pad extra frames needed
        video_padded = np.zeros((nb, h, w, c))
        video_padded[:totalf, ...] = video
        # Reshape into batches
        video_batch = video_padded.reshape(-1, nf, h, w, c)
        # Normalize to inference
        video_batch = self.data.test_gen.normalize(video_batch)
        # Return video without padded sample
        if len(video_batch) >= 2:
            video_batch = video_batch[:-1]
        return video_batch

    def _logits_from_video(self, model, video_path):
        # Load video
        video = self._split_video_in_batches(video_path, self.max_frames, self.data.input_shape[0])
        # Predict on each chunk frames
        logits = model.predict_on_batch(video)
        # Average those predictions
        logits = np.average(logits, axis=0)
        # Return average logits
        return logits

    def _preds_from_logits(self, logits, top_n=5):
        # Calculate softmax on logits
        predictions = np.exp(logits) / np.sum(np.exp(logits))
        # Sort top predictions
        sorted_indices = np.argsort(predictions)[::-1]
        # Display top predictions
        preds = []
        for index in sorted_indices[:top_n]:
            preds.append((predictions[index], self.data.key2class[index], index))
        return preds

    def _predict_on_test(self):
        preds = []
        for video in tqdm(self.data.test_gen.video_ids):
            logits = self._logits_from_video(self.model, video)
            pred = self._preds_from_logits(logits, top_n=1)[0][-1]
            preds.append(pred)
        self.y_pred = np.array(preds)
        self.y_true = np.argmax(self.data.test_gen.labels, axis=1)
        self.class_names = [class_name for k, class_name in self.data.key2class.items()]

    def plot_confusion_matrix(self, figsize=(30, 30)):
        confusion = confusion_matrix(self.y_true, self.y_pred)
        confusion_m = confusion.astype('float') / confusion.sum(axis=1)[:, np.newaxis]
        plt.figure(figsize=figsize)
        ax = sns.heatmap(confusion_m, cmap="YlGnBu",
                         annot=confusion, fmt='d', vmin=0, vmax=1)
        ax.yaxis.set_ticklabels(self.class_names, rotation=0, ha='right', fontsize=10)
        ax.xaxis.set_ticklabels(self.class_names, rotation=90, ha='right', fontsize=10)
        plt.xlabel('Predictions', fontsize=13)
        plt.ylabel('Labels', fontsize=13)
        plt.show()

    def report(self):
        print('Accuracy: {}'.format((self.y_true == self.y_pred).mean()))
        print(classification_report(self.y_true, self.y_pred, 
                                    target_names=self.class_names))
