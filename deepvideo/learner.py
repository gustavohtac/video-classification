from .utils import *
from .find_lr import LRFinder
from .cycle_lr import CyclicLR

from tensorflow.python.keras import optimizers
from keras.utils.np_utils import to_categorical
from tensorflow.python.keras.models import Model
from tensorflow.python.keras.callbacks import ModelCheckpoint
from tensorflow.python.keras.models import load_model
from tensorflow.python.keras.utils import plot_model

import pickle
import matplotlib.pyplot as plt

class VideoLearner():
    '''
        fast.ai based learner
        workflow and good practices learned from fast.ai course
        page: https://www.fast.ai/
    '''

    def __init__(self, data, model):
        '''
            dataframe: expects dataloader object
            model: expects keras model
        '''
        self.data = data
        self.model = model
        self.setup()

    def _fit_model(self, epochs, callbacks):
        
        if self.data.train_tfrecord:
        # If using tfrecord as dataset
            current_history = self.model.fit(
                                self.data.train_tfrecord,
                                validation_data=self.data.val_tfrecord,
                                callbacks=callbacks,
                                epochs=epochs,
                                steps_per_epoch = len(self.data.train_gen), # Not really precise
                                validation_steps = len(self.data.test_gen),
                                verbose=1)
        else:
        # If using keras datagenerator as dataset
            current_history = self.model.fit_generator(
                                generator=self.data.train_gen,
                                validation=self.data.test_gen,
                                callbacks=callbacks,
                                epochs=epochs,
                                verbose=1)

        return current_history

    def setup(self, callbacks=None, loss='categorical_crossentropy'):
        '''
            Setup extra parameters
            callbacks: set custom callback
            loss: set custom loss
        '''
        self.loss = loss
        self.callbacks = callbacks
        self.history = None

    def lr_find(self, epochs=1, max_lr=10, min_lr=1e-5):
        '''
            Find optimal learning rate
            epochs: number of epochs
            max_lr: final learning rate value
            min_lr: starting learning rate value
        '''

        # Compile
        self.model.compile(loss=self.loss, optimizer='Adam', metrics=['acc'])
        # Find optimal learning rate
        self.recorder = LRFinder(self.model)
        self.recorder.find(self.data, min_lr, max_lr, epochs)
        
    def freeze_to(self, n):
        '''
            Freeze weights up to nth layer
        '''
        # Freeze layers
        for layer in self.model.layers[:n]:
            layer.trainable = False
        for layer in self.model.layers[n:]:
            layer.trainable = True
            
    def unfreeze(self):
        '''
            Unfreeze all layers
        '''
        self.freeze_to(0)

    def freeze(self):
        '''
            Freeze all but last 5 layers (Model head)
        '''
        self.freeze_to(-5)
        
    def summary(self):
        '''
            Display model summary
        '''
        self.model.compile(loss=self.loss, optimizer='Adam', metrics=['acc'])
        self.model.summary()
        
    def save_fig(self, name):
        '''
            Save model figure
        '''
        plot_model(self.model, 
                   to_file='{}.png'.format(name), 
                   show_shapes=True)
    
    def fit(self, epochs, lr):
        '''
            Fit model
            lr: setup learning rate
            epochs: number of epochs
        '''

        # Setup optimizer
        self.opt = optimizers.Adam(lr=lr)
        # Compile
        self.model.compile(loss=self.loss, optimizer=self.opt, metrics=['acc'])
        # Setup callbacks
        fit_callbacks = ifnone(self.callbacks, [])
        # Fit
        current_history = self._fit_model(epochs, fit_callbacks)
        # Append history
        self.history = combine_dicts(self.history, current_history.history)

    def fit_one_cycle(self, epochs, lr, mode='triangular'):
        '''
            Fit model with one cycle technique
            lr: setup learning rate
            epochs: number of epochs
        '''

        # Setup optimizer
        self.opt = optimizers.Adam(lr=lr)
        # Compile
        self.model.compile(loss=self.loss, optimizer=self.opt, metrics=['acc'])
        # Setup callbacks
        fit_callbacks = ifnone(self.callbacks, [])
        # Cyclic learning rate training
        fit_callbacks.append(CyclicLR(base_lr=lr * 0.1,
                                       max_lr=lr,
                                       step_size=len(self.data.train_gen.video_ids) * 5,
                                       mode=mode))
        # Fit
        current_history = self._fit_model(epochs, fit_callbacks)
        # Append history
        self.history = combine_dicts(self.history, current_history.history)

    def save(self, model_name):
        '''
            Save current model state
            model_name: output filename
        '''

        # Save model
        self.model.save('{}.hdf5'.format(model_name))

    def load(self, model_name):
        '''
            Loads model from file
        '''
        
        self.model = load_model('{}.hdf5'.format(model_name))
        
    def save_report(self, path):
        to_save = [self.history, (self.recorder.losses, self.recorder.lrs), self.data.key2class]
        with open(os.path.join(path, 'report.pkl'), 'wb') as f:
            pickle.dump(to_save, f)
        
    def plot_history(self, figsize=(14, 10)):
        '''
            Plot training history
        '''

        # summarize history for accuracy
        plt.figure(figsize=figsize)
        plt.plot(self.history['acc'])
        plt.plot(self.history['val_acc'])
        plt.title('Acurácia do Modelo')
        plt.ylabel('Acurácia')
        plt.xlabel('Época')
        plt.legend(['Treino', 'Validação'], loc='upper left')
        plt.show()

        # summarize history for loss
        plt.figure(figsize=figsize)
        plt.plot(self.history['loss'])
        plt.plot(self.history['val_loss'])
        plt.title('Custo')
        plt.ylabel('Custo')
        plt.xlabel('Época')
        plt.legend(['Treino', 'Validação'], loc='upper left')
        plt.show()