from .i3d_inception import Inception_Inflated3d, conv3d_bn

from tensorflow.python.keras.models import Model
from tensorflow.python.keras.layers import AveragePooling3D
from tensorflow.python.keras.layers import Dropout
from tensorflow.python.keras.layers import Reshape
from tensorflow.python.keras.layers import Lambda
from tensorflow.python.keras.layers import Activation
from tensorflow.python.keras.layers import Dense
from tensorflow.python.keras.layers import GlobalAveragePooling2D
from tensorflow.python.keras.layers import Input

from tensorflow.python.keras.applications.inception_v3 import InceptionV3
from tensorflow.python.keras.applications.resnet50 import ResNet50

from tensorflow.python.keras import backend as K

import tensorflow as tf

def I3d_pretrained(
				classes,
				input_shape=(16, 244, 244, 3),
                dropout_prob=0.0,
                weights='rgb_imagenet_and_kinetics'
                ):

	'''
		A Inception Inflated3d wrapper designed for transfer learning
	'''

	i3d_basemodel = Inception_Inflated3d(
	                include_top=False,
	                weights=weights,
	                input_shape=input_shape)

	#Adding custom Layers 
	x = i3d_basemodel.output
	x = Dropout(dropout_prob)(x)

	x = conv3d_bn(x, classes, 1, 1, 1, padding='same', 
	              use_bias=True, use_activation_fn=False, use_bn=False, name='Conv3d_6a_1x1')
	 
	num_frames_remaining = int(x.shape[1])
	x = Reshape((num_frames_remaining, classes))(x)

	# logits (raw scores for each class)
	x = Lambda(lambda x: K.mean(x, axis=1, keepdims=False),
	           output_shape=lambda s: (s[0], s[2]))(x)

	x = Activation('softmax', name='prediction')(x)

	# creating the final model 
	i3d_pretrained = Model(inputs = i3d_basemodel.input, outputs = x, name='i3d_transfer')

	return i3d_pretrained

def Inception(
			classes,
			input_shape=(1, 244, 244, 3),
            dropout_prob=0.4,
            weights='imagenet'
	        ):
	input_tensor = Input(shape=input_shape)
	# Squeeze input tensor
	squeezed = Lambda(lambda x: K.squeeze(x, 1))(input_tensor)
	# Load Inception as base model
	base_model = InceptionV3(input_tensor=squeezed, weights=weights, include_top=False)
	# Define base model and flatten last layer
	x = base_model.output
	x = GlobalAveragePooling2D()(x)
	# Add a dense layer
	x = Dense(1024, activation='relu')(x)
	# Add final predictions layer
	x = Dropout(dropout_prob)(x)
	predictions = Dense(classes, activation='softmax')(x)
	# Define final model
	model = Model(inputs=input_tensor, outputs=predictions)

	return model

def ResNet(
			classes,
			input_shape=(1, 244, 244, 3),
            dropout_prob=0.4,
            weights='imagenet'
	        ):
	input_tensor = Input(shape=input_shape)
	# Squeeze input tensor
	squeezed = Lambda(lambda x: K.squeeze(x, 1))(input_tensor)
	# Load Inception as base model
	base_model = ResNet50(input_tensor=squeezed, weights=weights, include_top=False)
	# Define base model and flatten last layer
	x = base_model.output
	x = GlobalAveragePooling2D()(x)
	# Add a dense layer
	x = Dense(1024, activation='relu')(x)
	# Add final predictions layer
	x = Dropout(dropout_prob)(x)
	predictions = Dense(classes, activation='softmax')(x)
	# Define final model
	model = Model(inputs=input_tensor, outputs=predictions)

	return model