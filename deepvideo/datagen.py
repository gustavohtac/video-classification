import tensorflow.python.keras
from PIL import Image
import numpy as np
import os

class VideoGenerator(tensorflow.keras.utils.Sequence):
    '''
        Keras video generator
        https://stanford.edu/~shervine/blog/keras-how-to-generate-data-on-the-fly
    '''
    
    def __init__(self,
                 video_ids,
                 labels,
                 path,
                 batch_size = 32,
                 n_classes = 101,
                 input_shape = (16, 244, 244, 3),
                 shuffle = True,
                 data_augmentation = None,
                 n_workers = 12
                 ):
        
        self.video_ids = video_ids
        self.labels = labels
        self.path = path
        self.shuffle = shuffle
        self.input_shape = input_shape
        self.batch_size = batch_size
        self.n_classes = n_classes
        self.data_aug = data_augmentation
        self.on_epoch_end()

    
    def __len__(self):
        'Denotes the number of batches per epoch'
        return int(np.floor(len(self.video_ids) / self.batch_size))
    
    def __getitem__(self, index):
        'Generate one batch of data'
        # Generate indexes of the batch
        indexes = self.indexes[index*self.batch_size:(index+1)*self.batch_size]

        # Find list of IDs
        video_ids_temp = self.video_ids[indexes]

        # Generate data
        x, y = self.__data_generation(video_ids_temp, indexes)

        return x, y
    
    def on_epoch_end(self):
        'Updates indexes after each epoch'
        self.indexes = np.arange(len(self.video_ids))
        if self.shuffle == True:
            np.random.shuffle(self.indexes)
    
    def __data_generation(self, video_ids_temp, indexes):
        'Generates data containing batch_size samples'
        x = list(map(self.video_reader, video_ids_temp))
        x = np.stack(x, axis=0)
        
        x = self.normalize(x)
        
        y = self.labels[indexes]
        
        if self.data_aug:
            x = self._augment(x)
        
        return x, y
    
    def video_reader(self, video, all_frames=False, max_frames=None):
        '''
        n_frames: number of contiguous frames to be taken
        video: video in which frames will be extracted
        '''       
        # Get selected video path
        video_path = os.path.join(self.path, video)
        # Calculate number of frames in the whole video
        total_frames = len(os.listdir(video_path))
        # Set number of frames
        n_frames = total_frames-1 if all_frames else self.input_shape[0]
        # Set maximum frames
        if max_frames:
            n_frames = min(max_frames, n_frames)
        # Choose a random base frame
        base_idx = 1 if all_frames else np.random.randint(1, total_frames - n_frames)
        # Initialize video output
        video_out = np.zeros((n_frames, *self.input_shape[1:]))    
        # Read and return the selected frames  
        for offset_idx in range(n_frames):
            frame_path = os.path.join(video_path, 
                                      'image_{:05d}.jpg'.format(base_idx+offset_idx))
            video_out[offset_idx] = self._load_image(frame_path)
        return video_out
            
    def _load_image(self, path):
        with open(path, 'rb') as f:
            with Image.open(f) as img:
                img = img.convert('RGB')
                img = img.resize((self.input_shape[-2], self.input_shape[-3]))
                return img
    
    def normalize(self, x, mode=0):
        if mode == 0:
            x /= 127.5
            x -= 1.
        else:
            x /= 255
        return x
    
    def _augment(self, video):
        bs, nf, h, w, c = video.shape
        video = video.transpose(0, 2, 3, 1, 4)
        video = video.reshape(bs, h, w, c*nf)
        video = self.data_aug.augment_images(video)
        video = video.reshape(bs, h, w, nf, c)
        video = video.transpose(0, 3, 1, 2, 4)
        return video