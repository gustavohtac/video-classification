from matplotlib import pyplot as plt
import math
import os
from tensorflow.python.keras.callbacks import LambdaCallback
from tensorflow.python.keras import backend as K
import numpy as np


class LRFinder:
    """
    Plots the change of the loss function of a Keras model when the learning rate is exponentially increasing.
    See for details:
    https://towardsdatascience.com/estimating-optimal-learning-rate-for-a-deep-neural-network-ce32f2556ce0
    """
    def __init__(self, model):
        self.model = model
        self.losses = []
        self.lrs = []
        self.best_loss = 1e9
        self.verbose = 1

    def on_batch_end(self, batch, logs):
        # Log the learning rate
        lr = K.get_value(self.model.optimizer.lr)
        self.lrs.append(lr)

        # Log the loss
        loss = logs['loss']
        self.losses.append(loss)

        # Check whether the loss got too large or NaN
        if math.isnan(loss) or loss > self.best_loss * 4:
            self.model.stop_training = True
            return

        if loss < self.best_loss:
            self.best_loss = loss

        # Increase the learning rate for the next batch
        lr *= self.lr_mult
        K.set_value(self.model.optimizer.lr, lr)

    def _fit_model(self, data, epochs, callbacks):
        if data.train_tfrecord:
        # If using tfrecord as dataset
            self.model.fit(
                        data.train_tfrecord,
                        callbacks=callbacks,
                        epochs=epochs,
                        steps_per_epoch = len(data.train_gen), # Not really precise
                        verbose=self.verbose)
        else:
        # If using keras datagenerator as dataset
            self.model.fit_generator(
                        generator=data.train_gen,
                        callbacks=callbacks,
                        epochs=epochs,
                        verbose=self.verbose)


    def find(self, data, start_lr, end_lr, epochs=1):
        self.lr_mult = (float(end_lr) / float(start_lr)) ** (float(1) / float(len(data.train_gen)))

        # Save weights into a file
        self.model.save_weights('tmp.h5')

        # Remember the original learning rate
        original_lr = K.get_value(self.model.optimizer.lr)

        # Set the initial learning rate
        K.set_value(self.model.optimizer.lr, start_lr)

        callback = LambdaCallback(on_batch_end=lambda batch, logs: self.on_batch_end(batch, logs))

        self._fit_model(data, epochs, [callback])

        # Restore the weights to the state before model fitting
        self.model.load_weights('tmp.h5')
        
        # Delete tmp weights
        os.remove("tmp.h5")

        # Restore the original learning rate
        K.set_value(self.model.optimizer.lr, original_lr)

    def plot_loss(self, sma=5, n_skip_beginning=5, n_skip_end=2):
        """
        Plots the loss.
        Parameters:
            n_skip_beginning - number of batches to skip on the left.
            n_skip_end - number of batches to skip on the right.
        """
        loss = self.moving_average(self.losses[n_skip_beginning:-n_skip_end], sma)
        lrs = self.lrs[n_skip_beginning+sma-1:-n_skip_end]
        
        plt.figure()
        plt.ylabel("loss")
        plt.xlabel("learning rate (log scale)")
        plt.plot(lrs, loss)
        plt.xscale('log')
        
    def moving_average(self, a, n=5) :
        ret = np.cumsum(a, dtype=float)
        ret[n:] = ret[n:] - ret[:-n]
        return ret[n - 1:] / n