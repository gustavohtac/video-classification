'''
    Adapted from kenshohara's 3D ResNets for Action Recognition
    paper: https://arxiv.org/abs/1711.09577
    github: https://github.com/kenshohara/3D-ResNets-PyTorch
'''

'''
    WORK IN PROGRESS
'''

import os
import sys
import subprocess
import cv2 as cv
import numpy as np
from tqdm import tqdm


class VideoConverter():
  
  def __init__(self, video_dataset_path, frames_dataset_path, flow_dataset_path):
    self.video_dataset_path = video_dataset_path
    self.frames_dataset_path = frames_dataset_path
    self.flow_dataset_path = flow_dataset_path
    
  def _class_process(self, dir_path, dst_dir_path, class_name):
    class_path = os.path.join(dir_path, class_name)
    if not os.path.isdir(class_path):
      return

    dst_class_path = os.path.join(dst_dir_path, class_name)
    if not os.path.exists(dst_class_path):
      os.mkdir(dst_class_path)

    for file_name in os.listdir(class_path):
      if '.avi' not in file_name:
        continue
      name, ext = os.path.splitext(file_name)
      dst_directory_path = os.path.join(dst_class_path, name)

      video_file_path = os.path.join(class_path, file_name)
      try:
        if os.path.exists(dst_directory_path):
          if not os.path.exists(os.path.join(dst_directory_path, 'image_00001.jpg')):
            subprocess.call('rm -r \"{}\"'.format(dst_directory_path), shell=True)
            os.mkdir(dst_directory_path)
          else:
            continue
        else:
          os.mkdir(dst_directory_path)
      except:
        continue
      cmd = 'ffmpeg -i \"{}\" -vf scale=-1:240 \"{}/image_%05d.jpg\"'.format(video_file_path, dst_directory_path)
      subprocess.call(cmd, shell=True)

  def _make_flow(self, video_path, flow_path):
    # Initialize video reader
    cap = cv.VideoCapture(video_path)
    # Initialize frame counter
    num_frame = 1
    # Reads first frame
    ret, frame1 = cap.read()
    prvs_frame = cv.cvtColor(frame1, cv.COLOR_BGR2GRAY)
    # Create empty channel
    flow_image = np.zeros(frame1.shape, 'float32')
    # Set opticalflow
    optical_flow = cv.DualTVL1OpticalFlow_create()
    frames = []
    try:
        while True:
            # Reads next frame
            ret, frame2 = cap.read()
            # Return if couldn't read next frame
            if not ret:
                break
            next_frame = cv.cvtColor(frame2, cv.COLOR_BGR2GRAY)
            # Calculates optical flow 
            flow = optical_flow.calc(prvs_frame, next_frame, None)
            np.clip(flow, -20, 20, out=flow)
            flow_image[:,:,0:2] = flow * (1./40.) + 0.5
            # Save optical flow image
            cv.imwrite('{}/image_{:05d}.jpg'.format(flow_path, num_frame), flow_image.astype('uint8'))
            # Save flow frame into video
            frames.append(flow)
            # Updates previous frame
            prvs_frame = next_frame
            # Updates frame counter
            num_frame += 1
    finally:
        cap.release()
    return np.array(frames, 'float32') * (1./40.) + 0.5

  def convert2frames(self):
    if not os.path.exists(self.frames_dataset_path):
      os.mkdir(self.frames_dataset_path)
    for class_name in tqdm(os.listdir(self.video_dataset_path)):
      self._class_process(self.video_dataset_path, 
                          self.frames_dataset_path,
                          class_name)

  def convert2flow(self, dir_path, dst_dir_path):
    for class_name in os.listdir(dir_path):
      self._class_process(dir_path, dst_dir_path, class_name)

