'''
    based on fast.ai and https://github.com/tensorflow/hub/blob/master/examples/colab/action_recognition_with_tf_hub.ipynb
'''


from .datagen import VideoGenerator
from .buildtfrecord import TFRecordDataset
from tensorflow.python.keras.utils import to_categorical
import numpy as np
import matplotlib.pyplot as plt
import imageio
from IPython import display
import os


class VideoLoader():
    
    def __init__(self, batch_size, input_shape=(16, 244, 244, 3), workers=6, data_aug=None, label_mapping=None):
        self.batch_size = batch_size
        self.workers = workers
        self.input_shape = input_shape
        self.train_gen = None
        self.test_gen = None
        self.data_aug = data_aug
        self.label_mapping = label_mapping
        self.use_keras_as_generator()
        
    def from_df(self, df):
        # TODO
        # Load from dataframe paths
        return 0
 
    def from_folder(self, path):
        # TODO
        # Load from train, test folders
        return 0

    def from_list(self, path, train, test):
        # Path
        self.path = path

        # Unpack
        (x_train, y_train) = train
        (x_test, y_test) = test
        
        # Convert to numpy
        x_train = np.array(x_train)
        x_test = np.array(x_test)
        
        if self.label_mapping:
            # Load mapping from file
            self._get_mapping_from_file()
        else:
            # Define proper mapping
            self.class2key = {v:k for k,v in enumerate(sorted(set(y_train)))}
            self.key2class = {v:k for k,v in self.class2key.items()}
            self.n_classes = len(self.class2key)

        # Map to one hot encoding
        y_train = [self.class2key[y] for y in y_train]
        y_train = to_categorical(y_train, num_classes=self.n_classes)
        
        y_test = [self.class2key[y] for y in y_test]
        y_test = to_categorical(y_test, num_classes=self.n_classes)

        # Train generator
        self.train_gen = VideoGenerator(video_ids=x_train,
                                        labels=y_train,
                                        path=path, 
                                        batch_size=self.batch_size,
                                        input_shape=self.input_shape,
                                        n_classes=self.n_classes,
                                        shuffle=True,
                                        data_augmentation = self.data_aug)
        
        # Test generator
        self.test_gen =  VideoGenerator(video_ids=x_test,
                                        labels=y_test, 
                                        path=path,
                                        batch_size=self.batch_size,
                                        input_shape=self.input_shape,
                                        n_classes=self.n_classes,
                                        shuffle=False)

    def build_tfrecord_dataset(self, tfrecords_path, n_epochs):
        # Setup training and validation paths
        train_path = os.path.join(tfrecords_path, 'train')
        val_path = os.path.join(tfrecords_path, 'val')

        # Make dir if it does not exists
        if not os.path.exists(tfrecords_path):
            os.makedirs(train_path)
            os.makedirs(val_path)
            print('Making tfrecord dataset at {} and {}'
                .format(train_path, val_path))

        # Setup tfdataset and build train dataset
        tfdataset = TFRecordDataset(self, train_path)
        tfdataset.build_tfrecords_dataset(self.train_gen, n_epochs)

        # Setup tfdataset and build val dataset
        tfdataset = TFRecordDataset(self, val_path)
        tfdataset.build_tfrecords_dataset(self.test_gen, 1)

        # Setup generators from tfdataset
        self.use_tfrecord_as_generator(tfrecords_path)

    def use_tfrecord_as_generator(self, tfrecords_path):

        # Setup training and validation paths
        train_path = os.path.join(tfrecords_path, 'train')
        val_path = os.path.join(tfrecords_path, 'val')
       
        # Setup tfdataset and build train generator
        tfdataset = TFRecordDataset(self, train_path)
        self.train_tfrecord = tfdataset.get_tfdata_generator()

        # Setup tfdataset and build val generator
        tfdataset = TFRecordDataset(self, val_path)
        self.val_tfrecord = tfdataset.get_tfdata_generator()

    def use_keras_as_generator(self):
        self.train_tfrecord = None
        self.val_tfrecord = None
        
    def show_videos(self, n=12):
        
        # Get a random batch
        videos, labels = self.get_batch()
        
        # Show gifs
        self._animate(videos[:n])

    def show_frames(self, max_videos=5, n_frames=5 ,figsize=(20,20)):
        
        # Get a random batch
        videos, labels = self.get_batch()

        # Setup grid
        n_rows = min(self.batch_size, max_videos)
        colums = np.linspace(0, videos.shape[1]-1, n_frames).round().astype('uint16')

        # Plot
        plt.figure(figsize=figsize)
        n_plot = 0
        for r in range(n_rows):
            for c in colums:
                plt.subplot(n_rows, n_frames, n_plot+1)
                plt.imshow(videos[r][c])
                plt.title(labels[r])
                n_plot += 1
        plt.show()
        
    def get_batch(self):
        
        # Shuffle batch
        self.train_gen.on_epoch_end()

        # Get one batch of data
        videos, labels = next(iter(self.train_gen))

        # Normalize video
        videos += 1.
        videos *= 127.5
        videos = videos.astype('uint8')

        # Map y into string label
        labels = np.argmax(labels, axis=1)
        labels = [self.key2class[l] for l in labels]
        
        return videos, labels
            
    def _make_html(self, image):
        return '<img src="{}" style="display:inline;margin:1px"/>'.format(image)

    def _animate(self, images):
        if not os.path.exists('./anim'):
            os.makedirs('./anim')
        gifs = []
        for gif_id in range(len(images)):
            image = images[gif_id]
            current_gif = './anim/animation_{}.gif'.format(gif_id)
            imageio.mimsave(current_gif, image, fps=25)
            gifs.append(current_gif)
        html_string = ' '.join([self._make_html(x) for x in gifs])
        display.display(display.HTML(html_string))
        
    def _get_mapping_from_file(self):
        with open(self.label_mapping, 'r') as f:
            lines = f.read().splitlines() 
            self.key2class = {}
            self.class2key = {}
            for line in lines:
                idx, cat = line.split(' ')
                idx = int(idx) - 1
                self.key2class[idx] = cat
                self.class2key[cat] = idx
        self.n_classes = len(self.class2key)