import os
from datetime import datetime
import numpy as np
import pandas as pd

from tensorflow.python.keras.utils import to_categorical
from sklearn.model_selection import train_test_split

from imgaug import augmenters as iaa
import imgaug as ia


def get_hmdb_dataset(path='../../HMDB-51-dataset/hmdb-51-splits.csv',
                split_number=1):
    
    # Read splits data
    df = pd.read_csv(path)
    # Filter by split number
    df = df[df['split'] == split_number]
    # Select sets
    X_train = df['video'][df['set_str'] == 'train']
    y_train = df['label'][df['set_str'] == 'train']
    X_test = df['video'][df['set_str'] == 'test']
    y_test = df['label'][df['set_str'] == 'test']

    return (X_train, y_train), (X_test, y_test)

def get_ucf_dataset(base_path='../../UCF-101-dataset/ucfTrainTestlist',
                split_number=1):
    
    # Assert split is in valid range
    assert (split_number >= 1) and (split_number <= 3), \
    'Invalid split. Choose from 1, 2 or 3 folds.'
    
    # Get training data
    with open(base_path+'/trainlist{:02d}.txt'.format(split_number), 'r') as train_file:
        train_videos = []
        train_label = []
        for video in train_file:
            train, label = video.split(' ')
            train_videos.append(train.split('.')[0])
            train_label.append(video.split('/')[0])

    # Get test data
    with open(base_path+'/mod_testlist{:02d}.txt'.format(split_number), 'r') as test_file:
        test_videos = []
        test_label = []
        for video in test_file:
            test, label = video.split(' ')
            test_videos.append(test.split('.')[0])
            test_label.append(video.split('/')[0])
    
    X_train = train_videos 
    X_test = test_videos
    y_train = train_label
    y_test = test_label
    
    return (X_train, y_train), (X_test, y_test)

def get_augmentations(fliplr=0.5, flipud=0.0, crop=0.15, affine=0.15):
    sometimes_crop = lambda aug: iaa.Sometimes(crop, aug)
    sometimes_affine = lambda aug: iaa.Sometimes(affine, aug)
    aug = iaa.Sequential(
            [
                # apply the following augmenters to most images
                iaa.Fliplr(fliplr), # horizontally flip 50% of all images
                iaa.Flipud(flipud), # vertically flip 20% of all images
                # crop images by -10% to 10% of their height/width
                sometimes_crop(iaa.CropAndPad(
                    percent=(-0.1, 0.1),
                    pad_mode=ia.ALL,
                    pad_cval=(0, 255)
                )),
                sometimes_affine(iaa.Affine(
                    scale={"x": (0.9, 1.1), "y": (0.9, 1.1)}, # scale images to 90-110% of their size, individually per axis
                    translate_percent={"x": (-0.1, 0.1), "y": (-0.1, 0.1)}, # translate by -10 to +10 percent (per axis)
                    rotate=(-10, 10), # rotate by -10 to +10 degrees
                    shear=(-5, 5), # shear by -5 to +5 degrees
                    order=[0, 1], # use nearest neighbour or bilinear interpolation (fast)
                    cval=(0, 255), # if mode is constant, use a cval between 0 and 255
                    mode=ia.ALL # use any of scikit-image's warping modes (see 2nd image from the top for examples)
                ))
            ],
            random_order=True
        )
    return aug

def ifnone(a, b):
    "`a` if `a` is not None, otherwise `b`. from fast.ai"
    return b if a is None else a

def combine_dicts(d1, d2):
    if d1 is None:
        return d2
    elif d2 is None:
        return d1
    else:
        return {key: d1[key] + d2[key] for key in d1}